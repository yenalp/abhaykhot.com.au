var APP = (function($,utils,googlemap){
	var showMenu = function(){
		$(".navbar").toggleClass("showmenu");

		$(".menu-trigger").siblings().children("a:not(.new-menu)").addClass("dark-green");
	};

	var hideMenu = function(){
		$(".navbar").toggleClass("showmenu");

		$(".menu-trigger").siblings().children("a:not(.new-menu)").removeClass("dark-green");
	};

	var initCarousel = function(){
		if($(window).width() > 768){
			//$( ".carousel-inner" ).load("/desktop-carousel");
		}else{
			//$( ".carousel-inner" ).load("/mobile-carousel");
		}

		//Cases carousel
		$('.carousel').carousel({
			interval: 10000       //auto-scroll in 15s
		});
		$(".left-arrow").on("click", function(){
			$('.carousel').carousel("prev");
			activeSlideFade();
		});
		$(".right-arrow").on("click",  function(){
			$('.carousel').carousel("next");
			activeSlideFade();
		});
	};

	var toggleMenuStyle = function(){
		var top = $(document).scrollTop();
		var cap =  $('.menu-divde').offset().top - 85;

		if($(window).width() < 1200){
			cap = 370;

			if($(window).width() < 768){
				cap = 500;
			}
		}

		if(top > 10){
			$(".menu-shade").css({
				"background": "#fff",
				"border-bottom": "1px solid #eee"
			});

			$(".main-item").addClass("new-menu");
			$(".header-logo-colorful").fadeIn("fast");
			//$(".header-logo").addClass("header-logo-colorful");
			$(".navbar-default .navbar-toggle .icon-bar").addClass("blue-burger");
		}else{
			$(".menu-shade").css({
				"background": "none",
				"border-bottom": "none"
			});

			$(".main-item").removeClass("new-menu");
			$(".header-logo-colorful").fadeOut("fast");
			//$(".header-logo").removeClass("header-logo-colorful");
			$(".navbar-default .navbar-toggle .icon-bar").removeClass("blue-burger");
		}
	};

	var activeSlideFade = function(){
		var $cur = $(".carousel-inner .item.active");
		$cur.animate({
			opacity: 0
		},500, function(){
			setTimeout(function(){
				$cur.animate({
					opacity: 1
				}, 50)
			}, 1000);
		})
	};

	var movingBg = function(){
		var top = $(document).scrollTop();
		/*if($(window).width() > 1200){
			if(top > 0 && top < 660){
				$(".main-banner-image").css({
					"margin-top": -1 * top * 0.05 + "px"
				});
			}else if(top == 0){
				$(".main-banner-image").css({
					"margin-top": 0
				});
			}
		}*/
		toggleMenuStyle();
	};

	var movePageTo = function(e){
		switch($(e.target).html()){
			case "ABOUT US":
				$('html, body').animate({
	        scrollTop: $('.about').offset().top - 60
	      }, function(){
	      	activeNav($(".about-link"));
	      });
				break;
			case "SERVICES":
				$('html, body').animate({
	        scrollTop: $('.services').offset().top
	      }, function(){
	      	activeNav($(".services-link"));
	      });
				break;
			case "WORK":
				$('html, body').animate({
	        scrollTop: $('.cases').offset().top - 120
	      }, function(){
	      	activeNav($(".work-link"));
	      });
				break;
			case "CONTACT":
				$('html, body').animate({
	        scrollTop: $('.contact').offset().top - 120
	      }, function(){
	      	activeNav($(".contact-link"));
	      });
				break;
		}

		$(".navbar-collapse").removeClass("in").addClass("collapse");
	};

	var activeNav = function($el){
		$("#main-nav li a").removeClass("active-link");
		$el.addClass("active-link");
	};

	var renderNav = function(){
		var section1Top = 0;
		var section2Top = $('.works').offset().top - 120;
		var section3Top = $('.about').offset().top;
		var section4Top = $('.services').offset().top;
		var section5Top = $('.contact').offset().top;

		if($(document).scrollTop() >= section1Top && $(document).scrollTop() < section2Top){
      _obj.activeNav($(".hello-link"));
    } else if ($(document).scrollTop() >= section2Top && $(document).scrollTop() < section3Top){
     	_obj.activeNav($(".case-link"));
    } else if ($(document).scrollTop() >= section3Top && $(document).scrollTop() < section4Top){
     	_obj.activeNav($(".features-link"));
    } else if ($(document).scrollTop() >= section4Top && $(document).scrollTop() < section5Top){
     	_obj.activeNav($(".pricing-link"));
    } else if ($(document).scrollTop() >= section5Top && $(document).scrollTop() < section6Top){
     	_obj.activeNav($(".partners-link"));
    } else if ($(document).scrollTop() >= section6Top){
      _obj.activeNav($(".contact-link"));
    }
	};

	var subscribeClient = function(){
		var email = $(".footer-email").val();

		if(email == "" || !utils.validateEmail(email)){
			$("#error-msg").empty().html(message.errorEmail);
		}else{
			/*$.ajax({
	      type: "POST",
				url: "https://4e83291fcfa32962ef9041a12c708dac-us2.api.mailchimp.com/2.0/lists/subscribe",
	      data:{
	      	apikey: "4e83291fcfa32962ef9041a12c708dac-us2",
	      	id: "77c5a118c8",
	        email: email,
	        merge_vars: {
	        	"Source": "Prospective"
	        },
	        double_optin: false
	      },

	      beforeSend: function(){
	      	$("#ajax-loader").show();
	      },

	      success: function(res) {
	      	$("#error-msg").empty();
	      	$(".footer-email, .email-btn").fadeOut("fast", function(){
						$("#success-msg").fadeIn("fast");
					});
					$("#ajax-loader").hide();
	      },

	      error: function(e){
	      	$("#error-msg").empty();
	      	$(".footer-email, .email-btn").fadeOut("fast", function(){
						$("#success-msg").fadeIn("fast");
					});
					$("#ajax-loader").hide();
	      }
	    });*/
			$.ajax({
	      type: "POST",
				url: "/subscribe-client",
	      data:{
	        email: email
	      },

	      beforeSend: function(){
	      	$("#ajax-loader").show();
	      },

	      success: function(res) {
	      	$("#error-msg").empty();
	      	$(".footer-email, .email-btn").fadeOut("fast", function(){
						$("#success-msg").fadeIn("fast");
					});
					$("#ajax-loader").hide();
	      },

	      error: function(e){
	      	$("#error-msg").empty();
	      	$(".footer-email, .email-btn").fadeOut("fast", function(){
						$("#success-msg").fadeIn("fast");
					});
					$("#ajax-loader").hide();
	      }
	    });
		}
	};

	var showEmail = function(){
		$("#success-msg").fadeOut("fast", function(){
			$(".footer-email").val("");
			$(".footer-email, .email-btn").fadeIn("fast");
		});
	};

	var subscribeMobileClient = function(){
		var email = $(".footer-mobile-email").val();

		if(email == "" || !utils.validateEmail(email)){
			$("#error-mobile-msg").empty().html(message.errorEmail);
		}else{
			$.ajax({
	      type: "POST",
				url: "/subscribe-client",
	      data:{
	        email: email
	      },

	      beforeSend: function(){
	      	$("#ajax-mobile-loader").show();
	      },

	      success: function(res) {
	      	$("#error-mobile-msg").empty();
	      	$(".footer-mobile-email, .email-mobile-btn").fadeOut("fast", function(){
						$("#success-mobile-msg").fadeIn("fast");
					});
					$("#ajax-mobile-loader").hide();
	      },

	      error: function(e){
	      	$("#error-mobile-msg").empty();
	      	$(".footer-mobile-email, .email-mobile-btn").fadeOut("fast", function(){
						$("#success-mobile-msg").fadeIn("fast");
					});
					$("#ajax-mobile-loader").hide();
	      }
	    });
		}
	};

	var showMobileEmail = function(){
		$("#success-mobile-msg").fadeOut("fast", function(){
			$(".footer-mobile-email").val("");
			$(".footer-mobile-email, .email-mobile-btn").fadeIn("fast");
		});
	};

	var message = {
		errorName: function(){
			return "Please enter a valid name.";
		},

		errorPhone: function(){
			return "Please enter a valid phone.";
		},

		errorEmail: function(){
			return "Please enter a valid email.";
		},

		errorMessage: function(){
			return "Please enter your message.";
		},

		success:function(){
			return "Thanks for your enquiry. We will get back to you shortly.";
		},

		successEmail: function(){
			return "Congratulations! You have subscribed to Arkade.";
		},
	};

	var mapInit = function(){
		var mapExists = document.getElementById("google-map");

		if(mapExists){
			var myOptions = {
		    zoom: 18,
		    center: new google.maps.LatLng(-37.813561, 144.960754),
		    mapTypeId: google.maps.MapTypeId.ROADMAP,
		    styles: googlemap.mappSettings
			};

			var map = new google.maps.Map(document.getElementById('google-map'), myOptions);

			var image = '../images/map-marker.png';
		  var myLatLng = new google.maps.LatLng(-37.813561, 144.960754);
		  var beachMarker = new google.maps.Marker({
		      position: myLatLng,
		      map: map,
		      icon: image
		  });
		}
	}

	var progressBarInit = function(){
		$(".services-page .ecommerce-sales li").each(function(index, el){
			var percentage = 55 + index * 10;
			$(el).find(".data-progress").css({
				"width": percentage + "%"
			});
		})
	};

	var showWorkCaption = function(){
		$(this).find(".work-caption").addClass("show-caption");
	};

	var hideWorkCaption = function(){
		$(this).find(".work-caption").removeClass("show-caption");
	};

	var showService = function(e){
		var detail = $(this).data("detail");
		var category = $(this).data("type");

		/*if(typeof detail !== typeof undefined && detail !== false){
			var $img = $("<img src=/images/Platforms/" + detail + " width='150'>");
			$(this).parents(".service-logos").next(".detail-img").empty().append($img);
		}*/
		
		if(typeof category !== typeof undefined && category !== false){
			var $caption = $("<h3 class='platform-caption'>" + category + "</h3>");
			$(this).parents(".service-logos").next(".detail-img").empty().append($caption);
		}
	};

	var hideService = function(e){
		$(this).parents(".service-logos").next(".detail-img").empty();
	};

	var showApproachText = function(){
		var ids = $(this).attr("id").split("-");
		$("#" + ids[0]).fadeIn("fast");
	};

	var hideApproachText = function(){
		var ids = $(this).attr("id").split("-");
		$("#" + ids[0]).fadeOut("fast");
	};

	var obj = {
		
		//public functions
		init: function(){
			//$(".menu-trigger").on("mouseover", showMenu);
			//$(".menu-trigger").on("mouseout", hideMenu);

			initCarousel();

			//Home Page scroll effect
			$(document).on("scroll", movingBg);
			$(".talk-btn").on("click", function(){
				$('html, body').animate({
	        scrollTop: $('.contact').offset().top - 120
	      }, function(){
	      	activeNav($(".contact-link"));
	      });
			});

			$("#main-nav li").on("click", movePageTo);

			//Subscribe Email
			$(".email-btn").on("click", subscribeClient);
			$(".email-mobile-btn").on("click", subscribeMobileClient);
			$("#refresh-email").on("click", showEmail);
			$("#refresh-mobile-email").on("click", showMobileEmail);

			/* Toggle Work Caption */
			$(".work-link").on("mouseover", showWorkCaption);
			$(".work-link").on("mouseout", hideWorkCaption);

			/* Servie platform hover */
			$(".desktop-carousel .service-logos li a").on("mouseover", showService);
			$(".desktop-carousel .service-logos li a").on("mouseout", hideService);

			/* Toggle approach text */
			$(".approach-circle, .approach-square").on("mouseover", showApproachText);
			$(".approach-circle, .approach-square").on("mouseout", hideApproachText);

			progressBarInit();
			mapInit();
		}
	};

	return obj;
}(jQuery,UTILS,GOOGLEMAP));

$(document).ready(function(){
	APP.init();
});

