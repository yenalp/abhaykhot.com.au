var UTILS = (function(){
	var obj = {
		validateEmail: function(email){
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	return re.test(email);
		},

		validPhone: function(phone){
			var re = /(\W|^)[(]{0,1}\d{2}[)]{0,1}[\s-]{0,1}\d{4}[\s-]{0,1}\d{4}(\W|$)/;
			return re.test(phone);
		}
	};

	return obj;
}());