var WORK = (function($){
	var parallexShow = function(){
		var top = $(document).scrollTop();

		$("img[id*='web-inside']").css({
			"margin-top": -1 * top * 0.9 + "px" 
		});

		$("img[id*='mobile-inside']").css({
			"margin-top": -1 * top * 0.3 + "px" 
		});

		$("img[id*='white-mobile-inside-2']").css({
			"margin-top": -1 * top * 0.1 + "px" 
		});

		//toggle Contact block
		var winH = $(window).height();
		if(top >= 714){
			$(".contact").css({
				"position": "fixed",
				"left": 0,
				"right": 0,
				"bottom": 0,
				"top": "inherit",
				"z-index": 9999
			});
		}else{
			$(".contact").css({
				"position": "absolute",
				"left": 0,
				"right": 0,
				"top": 359 + winH + "px",
				"height": 359 + "px",
				"z-index": 9999
			});
		}
	};

	var gallerySlide = function(){
		var curIndex = 0;
    var numberOfItems = $(".gallery img").length;
    $('.gallery img').eq(curIndex).fadeIn(1000);

    var infiniteLoop = setInterval(function(){
        $(".gallery img").eq(curIndex).fadeOut(1000);
        if(curIndex == numberOfItems -1){
            curIndex = 0;
        }else{
            curIndex++;
        }
        $(".gallery img").eq(curIndex).fadeIn(1000);
    }, 5000);
	};

	var obj = {
		//public functions
		init: function(){
			$(document).on("scroll", parallexShow);
			gallerySlide();
		}
	};

	return obj;
}(jQuery));

$(document).ready(function(){
	WORK.init();
});