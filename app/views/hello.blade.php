@extends('layouts.default')

@section('page_title')
@stop

@section('content')
    <section>
        <h4>Melbourne Orthopaedic Surgeons</h4>
        <p>
            170 Gipps Street<br />
            East Melbourne
        </p>
        <p>
            PH: 03 9928 6969 <br />
            Fax: 03 9928 6971
        </p>

        <h4>Victorian Orthopaedic Centre</h4>
        <p>
            64 Chapman Street<br />
            North Melbourne VIC 3051
        </p>
        <p>
            PH: 03 9322 3370 <br />
            Fax: 03 9329 4969
        </p>
        <p>
            <!-- Email: <a href="mailto: info@abhayknot.com.au">info@abhayknot.com.au</a> -->
        </p>

    </section>
@stop
