<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta id="viewport" name="viewport" content ="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>Abhay Knot | Orthopaedic Surgeon</title>
<meta name="description" content="Abhay Knot Orthopaedic Surgeon.">
<meta name="keywords" content="" />
<meta property="og:site_name" content="Abhay Knot Orthopaedic Surgeon"/>
<meta property="og:description" content="Abhay Knot Orthopaedic Surgeon."/>
<meta property="og:title" content="Abhay Knot: Orthopaedic Surgeon"/>
<meta property="og:url" content="http://abhayknot.com.au/" />
<meta property="og:type" content="business.business"/>
<meta property="place:location:latitude" content="-37.812214" />
<meta property="place:location:longitude" content="144.961793" />
<meta property="business:contact_data:street_address" content="170 Gipps Street" />
<meta property="business:contact_data:locality" content="East Melbourne" />
<meta property="business:contact_data:country_name" content="Australia" />
<meta property="business:contact_data:postal_code" content="3000" />
<meta property="business:contact_data:region" content="Victoria" />
<meta property="business:contact_data:email" content="hello@abhayknot.com.au" />
<meta property="business:contact_data:phone_number" content="+61 3 9928 6969" />
<meta property="business:contact_data:website" content="http://www.abhayknot.com.au" />
<meta name="author" content="Abhay Knot Orthopaedic Surgeon @ 2015">
<link rel="shortcut icon" href="favicon.ico" type="image">
<link href='https://fonts.googleapis.com/css?family=Parisienne' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6DSIyYlOUQ7w6jQcgxOALvxbe19howBI"></script>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PVPQL3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PVPQL3');</script>
<!-- End Google Tag Manager -->

<!--[if lt IE 9]>
      <script>
          location.href = "/ie"
      </script>
  <![endif]-->

<!-- CSS -->
{{ HTML::style('css/bootstrap.min.css') }}
{{ HTML::style('css/bootstrap-theme.min.css') }}
{{ HTML::style('css/style.css') }}

@yield('styles')

<!-- JS -->
{{HTML::script('js/jquery-1.10.2.min.js')}}
{{HTML::script('js/jquery-migrate-1.2.1.min.js')}}
{{HTML::script('js/bootstrap.min.js')}}
{{HTML::script('js/utils.js')}}
{{HTML::script('js/google-map-customize.js')}}
{{HTML::script('js/app.js')}}

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
