<div id="cases-carousel" class="carousel desktop-carousel slide carousel-fade" data-ride="carousel">
  <div class="carousel-inner">
    <div class="item active">
    	<div class="row">
        <div class="col-sm-4 project-block">
            <a class="iphone-imac review" href="http://www.review-australia.com/" target="_blank"></a>
            <h3>Review Website</h3>
            <p>The Review website features a fully responsive design that integrates eCommerce, store finder, loyalty redemption, click and collect, and live chat.</p>
            <ul class="service-logos">
                <li>
                    <a class="service-logo loyalty-service" data-type="Loyalty"></a>
                </li>
                <li>
                    <a class="service-logo comms-service" data-type="Comms & Content"></a>
                </li>
                <li>
                    <a class="service-logo ecommerce-service" data-type="eCommerce"></a>
                </li>
                <li>
                    <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                </li>
                <li>
                    <a class="service-logo integration-service" data-type="Integration"></a>
                </li>
            </ul>
            <a class="detail-img"></a>
            <a class="view-project hide-element">View Project</a>
        </div>
        <div class="col-sm-4 project-block">
            <a class="ipad" href="http://www.neupro.com" target="_blank"></a>
            <h3>Neupro App</h3>
            <p>The Neupro app is used by field reps to present up-to-date product information in an engaging format and provides detailed usage analytics.</p>
            <ul class="service-logos">
                <li>
                    <a class="service-logo comms-service" data-type="Comms & Content"></a>
                </li>
                <li>
                    <a class="service-logo app-service" data-type="Apps"></a>
                </li>
            </ul>
            <a class="detail-img"></a>
            <a class="view-project hide-element">View Project</a>
        </div>
        <div class="col-sm-4 project-block">
            <a class="iphone-imac politix" href="http://www.politix.com.au" target="_blank"></a>
            <h3>Politix Website</h3>
            <p>The Politix website features integration with Magento eCommerce, live chat, optimised checkout and automated email sends based on customer actions.</p>
            <ul class="service-logos">
                <li>
                    <a class="service-logo comms-service" data-detail="2_Mailchimp.jpg" data-type="Comms & Content"></a>
                </li>
                <li>
                    <a class="service-logo ecommerce-service" data-detail="7_Magento.jpg" data-type="eCommerce"></a>
                </li>
                <li>
                    <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                </li>
                <li>
                    <a class="service-logo integration-service" data-type="Integration"></a>
                </li>
            </ul>
            <a class="detail-img"></a>
            <a class="view-project hide-element">View Project</a>
        </div>
      </div>
    </div>

    <div class="item">
    	<div class="row">
        <div class="col-sm-4 project-block">
            <a class="iphone cotton-on" href="https://itunes.apple.com/au/app/cotton-on/id897303788?mt=8" target="_blank"></a>
            <h3>Cotton On Mobile App</h3>
            <p>The Cotton On App allows the brand to create geo-targeted content, products, offers and push notifications from country down to store level.</p>
            <ul class="service-logos">
                <li>
                    <a class="service-logo comms-service" data-type="Comms & Content"></a>
                </li>
                <li>
                    <a class="service-logo app-service" data-detail="15_Omneo.jpg" data-type="Apps"></a>
                </li>
                <li>
                    <a class="service-logo integration-service" data-type="Integration"></a>
                </li>
            </ul>
            <a class="detail-img"></a>
            <a class="view-project hide-element">View Project</a>
        </div>
        <div class="col-sm-4 project-block">
            <a class="imac mimco" href="http://www.mimco.com.au/default.aspx?Z=C&rnd=7ee2bf98-8ab2-4b31-95f5-b5864e7a3c86&action=displayaccount" target="_blank"></a>
            <h3>Mimco Loyalty Program</h3>
            <p>The "MIMCOLLECTIVE" loyalty program features behavioural rewards and triggered communications along with integrated social media and email content strategy.</p>
            <ul class="service-logos">
                <li>
                    <a class="service-logo comms-service" data-type="Comms & Content"></a>
                </li>
                <li>
                    <a class="service-logo ecommerce-service" data-type="eCommerce"></a>
                </li>
                <li>
                    <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                </li>
                <li>
                    <a class="service-logo integration-service" data-type="Integration"></a>
                </li>
            </ul>
            <a class="detail-img"></a>
            <a class="view-project hide-element">View Project</a>
        </div>
        <div class="col-sm-4 project-block">
            <a class="iphone witchery" href="https://itunes.apple.com/au/app/witchery/id738918006?mt=8" target="_blank"></a>
            <h3>Witchery Loyalty App</h3>
            <p>The Witchery loyalty app leverages their eCommerce site to allow purchasing within the app and the ability to redeem and receive rewards on and off line.</p>
            <ul class="service-logos">
                <li>
                    <a class="service-logo loyalty-service" data-type="Loyalty"></a>
                </li>
                <li>
                    <a class="service-logo comms-service" data-type="Comms & Content"></a>
                </li>
                <li>
                    <a class="service-logo ecommerce-service" data-type="eCommerce"></a>
                </li>
                <li>
                    <a class="service-logo app-service" data-type="Apps"></a>
                </li>
                <li>
                    <a class="service-logo integration-service" data-type="Integration"></a>
                </li>
            </ul>
            <a class="detail-img"></a>
            <a class="view-project hide-element">View Project</a>
        </div>
      </div>
    </div>

    <div class="item">
    	<div class="row">
        <div class="col-sm-4 project-block">
            <a class="iphone-imac loreal" href="http://salonlocator.lorealpro.com.au" target="_blank"></a>
            <h3>L'Oréal Salon Locator</h3>
            <p>The L’Oreal Salon Locator allows customers to search for their closest salon and filter by promotions, products and services in Australia and New Zealand.</p>
            <ul class="service-logos">
                <li>
                    <a class="service-logo comms-service" data-type="Comms & Content"></a>
                </li>
                <li>
                    <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                </li>
                <li>
                    <a class="service-logo integration-service" data-detail="20_Google.jpg" data-type="Integration"></a>
                </li>
            </ul>
            <a class="detail-img"></a>
            <a class="view-project hide-element">View Project</a>
        </div>
        <div class="col-sm-4 project-block">
            <div class="iphone skilled"></div>
            <h3>Skilled App</h3>
            <p>The Skilled App serves as a communication tool with their employees; featuring news and alerts, an in-app calendar and media centre.</p>
            <ul class="service-logos">
                <li>
                    <a class="service-logo comms-service" data-type="Comms & Content"></a>
                </li>
                <li>
                    <a class="service-logo app-service" data-type="Apps"></a>
                </li>
                <li>
                    <a class="service-logo integration-service" data-type="Integration"></a>
                </li>
            </ul>
            <a class="detail-img"></a>
            <a class="view-project hide-element">View Project</a>
        </div>
        <div class="col-sm-4 project-block">
            <a class="iphone-imac tonybianco" href="http://www.tonybianco.com.au" target="_blank"></a>
            <h3>Tony Bianco Website</h3>
            <p>The Tony Bianco website features a fully responsive design that integrates eCommerce, ‘Tony’s World’ loyalty program, wishlist and live chat.</p>
            <ul class="service-logos">
                <li>
                    <a class="service-logo loyalty-service" data-type="Loyalty"></a>
                </li>
                <li>
                    <a class="service-logo comms-service" data-type="Comms & Content"></a>
                </li>
                <li>
                    <a class="service-logo ecommerce-service" data-detail="7_Magento.jpg" data-type="eCommerce"></a>
                </li>
                <li>
                    <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                </li>
                <li>
                    <a class="service-logo integration-service" data-detail="1_Zendesk.jpg" data-type="Integration"></a>
                </li>
            </ul>
            <a class="detail-img"></a>
            <a class="view-project hide-element">View Project</a>
        </div>
      </div>
    </div>

    <div class="item">
        <div class="row">
            <div class="col-sm-4 project-block">
                <a class="iphone-imac politix2" href="http://www.politix.com.au" target="_blank"></a>
                <h3>Black Tie Campaign</h3>
                <p>The Politix Black Tie website featured a competition to win a $1000 wardrobe promoted through an integrated social media & email communications campaign.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo loyalty-service" data-type="Loyalty"></a>
                    </li>
                    <li>
                        <a class="service-logo comms-service" data-detail="2_Mailchimp.jpg" data-type="Comms & Content"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
            <div class="col-sm-4 project-block">
                <a class="iphone redken" href="http://looks.accessredken.com.au" target="_blank"></a>
                <h3>Redken Splashlights</h3>
                <p>The Redken Splashlights lookbook is a responsive website designed for mobile, where salons and their customers can view the latest trends.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo loyalty-service" data-type="Loyalty"></a>
                    </li>
                    <li>
                        <a class="service-logo comms-service" data-detail="3_Mandrill.jpg" data-type="Comms & Content"></a>
                    </li>
                    <li>
                        <a class="service-logo integration-service" data-type="Integration"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
            <div class="col-sm-4 project-block">
                <a class="imac metalicus" href="https://www.metalicus.com" target="_blank"></a>
                <h3>Metalicus Styling Events</h3>
                <p>The Metalicus Styling Workshops is an event booking system that includes payment integration via paypal, an automated email sequence, and waitlist.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo comms-service" data-detail="6_Responsys.jpg" data-type="Comms & Content"></a>
                    </li>
                    <li>
                        <a class="service-logo ecommerce-service" data-type="eCommerce"></a>
                    </li>
                    <li>
                        <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                    </li>
                    <li>
                        <a class="service-logo integration-service" data-type="Integration"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
        </div>
    </div>

    <div class="item">
        <div class="row">
            <div class="col-sm-4 project-block">
                <a class="imac redken" href="http://accessredken.com.au" target="_blank"></a>
                <h3>Access Redken</h3>
                <p>The Access Redken loyalty program allows monthly email offers to be sent to the Access Redken community that can be redeemed in participating Redken salons.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo loyalty-service" data-detail="3_Mandrill.jpg" data-type="Loyalty"></a>
                    </li>
                    <li>
                        <a class="service-logo comms-service" data-type="Comms & Content"></a>
                    </li>
                    <li>
                        <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                    </li>
                    <li>
                        <a class="service-logo integration-service" data-type="Integration"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
            <div class="col-sm-4 project-block">
                <a class="iphone tonybianco" href="https://itunes.apple.com/au/app/tony-bianco/id546281114?mt=8" target="_blank"></a>
                <h3>Tony Bianco Mobile App</h3>
                <p>The Tony Bianco Loyalty App allows members to view their rewards, receive segmented content, and shop in-app using the brand’s mobile eCommerce site.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo loyalty-service" data-type="Loyalty"></a>
                    </li>
                    <li>
                        <a class="service-logo comms-service" data-type="Comms & Content"></a>
                    </li>
                    <li>
                        <a class="service-logo ecommerce-service" data-detail="7_Magento.jpg" data-type="eCommerce"></a>
                    </li>
                    <li>
                        <a class="service-logo app-service" data-detail="15_Omneo.jpg" data-type="Apps"></a>
                    </li>
                    <li>
                        <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                    </li>
                    <li>
                        <a class="service-logo integration-service" data-type="Integration"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
            <div class="col-sm-4 project-block">
                <a class="imac review" href="http://www.review-australia.com/review-weddings" target="_blank"></a>
                <h3>Review Bridal Booking</h3>
                <p>Review Weddings is an online reservation system that allows a bridal party to book a VIP in store styling session at their desired location and time.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo comms-service" data-detail="6_Responsys.jpg" data-type="Comms & Content"></a>
                    </li>
                    <li>
                        <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                    </li>
                    <li>
                        <a class="service-logo integration-service" data-type="Integration"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
        </div>
    </div>

    <div class="item">
        <div class="row">
            <div class="col-sm-4 project-block">
                <a class="iphone-imac witchery" href="https://itunes.apple.com/au/app/witchery/id738918006?mt=8" target="_blank"></a>
                <h3>Witchery Loyalty</h3>
                <p>The Witchery Loyalty Program is a multi channel experience that offers personalised promotions and benefits to individual customers based on their behaviour.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo loyalty-service" data-type="Loyalty"></a>
                    </li>
                    <li>
                        <a class="service-logo comms-service" data-type="Comms & Content"></a>
                    </li>
                    <li>
                        <a class="service-logo ecommerce-service" data-type="eCommerce"></a>
                    </li>
                    <li>
                        <a class="service-logo app-service" data-type="Apps"></a>
                    </li>
                    <li>
                        <a class="service-logo integration-service" data-type="Integration"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
            <div class="col-sm-4 project-block">
                <div class="iphone-imac t2"></div>
                <h3>T2 Website</h3>
                <p>The T2 Website (U.S & U.K.) features a responsive design built on Magento Enterprise and supports localisation of content, pricing and inventory with existing SAP and Givex systems.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo comms-service" data-detail="19_Givex.jpg" data-type="Comms & Content"></a>
                    </li>
                    <li>
                        <a class="service-logo ecommerce-service" data-detail="7_Magento.jpg" data-type="eCommerce"></a>
                    </li>
                    <li>
                        <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                    </li>
                    <li>
                        <a class="service-logo integration-service" data-type="Integration"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
            <div class="col-sm-4 project-block">
                <a class="imac tonybianco2" href="http://www.tonybianco.com.au/join-tony-world" target="_blank"></a>
                <h3>Tony Bianco Loyalty</h3>
                <p>The Tony Bianco Loyalty Program integrates with POS and eCommerce to reward members for purchases and provide staff with information to assist with in store service.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo loyalty-service" data-type="Loyalty"></a>
                    </li>
                    <li>
                        <a class="service-logo comms-service" data-type="Comms & Content"></a>
                    </li>
                    <li>
                        <a class="service-logo ecommerce-service" data-detail="7_Magento.jpg" data-type="eCommerce"></a>
                    </li>
                    <li>
                        <a class="service-logo app-service" data-type="Apps"></a>
                    </li>
                    <li>
                        <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                    </li>
                    <li>
                        <a class="service-logo integration-service" data-detail="1_Zendesk.jpg" data-type="Integration"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
        </div>
    </div>

    <div class="item">
        <div class="row">
            <div class="col-sm-4 project-block">
                <a class="iphone-imac meganpark" href="http://meganpark.com.au" target="_blank"></a>
                <h3>Megan Park Website</h3>
                <p>The Megan Park website features a responsive design built using Shopify eCommerce and integrated with MailChimp email platform.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo comms-service" data-type="Comms & Content"></a>
                    </li>
                    <li>
                        <a class="service-logo ecommerce-service" data-type="eCommerce"></a>
                    </li>
                    <li>
                        <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                    </li>
                    <li>
                        <a class="service-logo integration-service" data-type="Integration"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
            <div class="col-sm-4 project-block">
                <a class="imac mmc" href="http://www.melbourneminingclub.com/events" target="_blank"></a>
                <h3>Melbourne Mining Club Booking</h3>
                <p>The MMC Event Booking Platform provides administrators with the tools to manage complex event bookings and includes built in insightful reporting and email automation.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo ecommerce-service" data-type="eCommerce"></a>
                    </li>
                    <li>
                        <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                    </li>
                    <li>
                        <a class="service-logo integration-service" data-type="Integration"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
            <div class="col-sm-4 project-block">
                <a class="iphone-imac metalicus" href="https://www.metalicus.com" target="_blank"></a>
                <h3>Metalicus Website</h3>
                <p>The Metalicus website integrating Magento eCommerce, features a fully responsive design, store locator, loyalty redemption, click and collect, and live chat.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo loyalty-service" data-type="Loyalty"></a>
                    </li>
                    <li>
                        <a class="service-logo comms-service" data-type="Comms & Content"></a>
                    </li>
                    <li>
                        <a class="service-logo ecommerce-service" data-type="eCommerce"></a>
                    </li>
                    <li>
                        <a class="service-logo app-service" data-type="Apps"></a>
                    </li>
                    <li>
                        <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                    </li>
                    <li>
                        <a class="service-logo integration-service" data-type="Integration"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
        </div>
    </div>

    <div class="item">
        <div class="row">
            <div class="col-sm-4 project-block">
                <a class="imac acs" href="http://www.cleckheatonsuperfine.com.au" target="_blank"></a>
                <h3>Cleckheaton Superfine Website</h3>
                <p>The Cleckheaton Superfine website features a fully responsive design with custom functionality built-in with Big Commerce and MailChimp email platform.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo comms-service" data-type="Comms & Content"></a>
                    </li>
                    <li>
                        <a class="service-logo ecommerce-service" data-type="eCommerce"></a>
                    </li>
                    <li>
                        <a class="service-logo integration-service" data-type="Integration"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
            <div class="col-sm-4 project-block">
                <a class="iphone coregas" href="https://itunes.apple.com/us/app/id920306683?mt=8" target="_blank"></a>
                <h3>Coregas App</h3>
                <p>The Coregas App allows staff to manage the movement of Coregas cylinders within their organisation and integrates with their existing platform infrastructure.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo comms-service" data-type="Comms & Content"></a>
                    </li>
                    <li>
                        <a class="service-logo app-service" data-type="Apps"></a>
                    </li>
                    <li>
                        <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                    </li>
                    <li>
                        <a class="service-logo integration-service" data-type="Integration"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
            <div class="col-sm-4 project-block">
                <a class="imac omneo" href="http://omneo.com.au" target="_blank"></a>
                <h3>Omneo CMS</h3>
                <p>The Omneo Mobile App CMS, built by Arkade, features custom and out-of-the-box integration, built-in segmentation, location targeting and behavioural reporting.</p>
                <ul class="service-logos">
                    <li>
                        <a class="service-logo loyalty-service" data-type="Loyalty"></a>
                    </li>
                    <li>
                        <a class="service-logo comms-service" data-type="Comms & Content"></a>
                    </li>
                    <li>
                        <a class="service-logo ecommerce-service" data-type="eCommerce"></a>
                    </li>
                    <li>
                        <a class="service-logo app-service" data-type="Apps"></a>
                    </li>
                    <li>
                        <a class="service-logo platform-service" data-type="Custom Platforms"></a>
                    </li>
                    <li>
                        <a class="service-logo integration-service" data-type="Integration"></a>
                    </li>
                </ul>
                <a class="detail-img"></a>
                <a class="view-project hide-element">View Project</a>
            </div>
        </div>
    </div>
  </div>

  <a class="nav-control left-arrow" data-slide="prev"></a>
  <a class="nav-control right-arrow" data-slide="next"></a>
</div>
