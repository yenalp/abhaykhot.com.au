<header>
  <div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="menu-shade"></div>
    <div class="container">
      <a class="pull-left header-logo" href="./"></a>
      <a class="pull-left header-logo-colorful" href="./"></a>

      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button>

      <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
        <ul id="main-nav" class="pull-right">
            <!-- <li>
                <a class="main-item work-link" onclick="ga('send', 'event', 'Work', 'Click Work', 'Work', 0);">WORK</a>
            </li>
            <li>
                <a class="main-item about-link" onclick="ga('send', 'event', 'Team', 'Click Team', 'Team', 0);">ABOUT US</a>
            </li>
            <li class="menu-trigger">
                <a class="main-item services-link" onclick="ga('send', 'event', 'Work', 'Click Work', 'Work', 0);">SERVICES</a>
                <ul class="sub-nav">
                  <li>
                    <a class="sub-link">Loyalty</a>
                  </li>
                  <li>
                    <a class="sub-link">Comms & Content</a>
                  </li>
                  <li>
                    <a class="sub-link">eCommerce</a>
                  </li>
                  <li>
                    <a class="sub-link">Apps</a>
                  </li>
                  <li>
                    <a class="sub-link">Custom Platforms</a>
                  </li>
                  <li>
                    <a class="sub-link">Integration</a>
                  </li>
                </ul>
            </li> -->
            <li>
                <a class="main-item contact-link" onclick="ga('send', 'event', 'Say Hello', 'Click Say Hello', 'Say Hello', 0);">CONTACT</a>
            </li>
        </ul>
      </nav>
    </div>
  </div>
</header>
