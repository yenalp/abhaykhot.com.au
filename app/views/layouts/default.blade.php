<!doctype html>
<html lang="en">
<head>
	@include('partials.head')
</head>
<body>
	@include('partials.top_nav')

	   @yield('content')

    @include('partials.footer')

</body>
</html>
