<!DOCTYPE HTML>
<html>
	<head>
		<title>Abhay Knot Orthopaedics Surgeon</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="{{ asset('/css/ie/html5shiv.js') }}"></script><![endif]-->
		<script src="{{ asset('/js/jquery.min.js') }}"></script>
		<script src="{{ asset('/js/jquery.dropotron.min.js') }}"></script>
		<script src="{{ asset('/js/skel.min.js') }}"></script>
		<script src="{{ asset('/js/skel-layers.min.js') }}"></script>
		<script src="{{ asset('/js/init.js') }}"></script>
		<noscript>
			<link rel="stylesheet" href="{{ asset('/css/skel.css') }}" />
			<link rel="stylesheet" href="{{ asset('/css/style.css') }}" />
			<link rel="stylesheet" href="{{ asset('/css/style-desktop.css') }}" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="{{ asset('/css/ie/v8.css') }}" /><![endif]-->
	</head>
	<body class="homepage">

		<!-- Header -->
			<div id="header-wrapper">
				<header id="header" class="container">

					<!-- Logo -->
						<div id="logo">
							<h1><a href="index.html">&#160;</a></h1>
							<span>&#160;</span>
						</div>

					<!-- Nav -->
						<!-- <nav id="nav">
							<ul>
								<li class="current"><a href="index.html">Welcome</a></li>
								<li>
									<a href="">Dropdown</a>
									<ul>
										<li><a href="#">Lorem ipsum dolor</a></li>
										<li><a href="#">Magna phasellus</a></li>
										<li>
											<a href="">Phasellus consequat</a>
											<ul>
												<li><a href="#">Lorem ipsum dolor</a></li>
												<li><a href="#">Phasellus consequat</a></li>
												<li><a href="#">Magna phasellus</a></li>
												<li><a href="#">Etiam dolore nisl</a></li>
											</ul>
										</li>
										<li><a href="#">Veroeros feugiat</a></li>
									</ul>
								</li>
								<li><a href="left-sidebar.html">Left Sidebar</a></li>
								<li><a href="right-sidebar.html">Right Sidebar</a></li>
								<li><a href="no-sidebar.html">No Sidebar</a></li>
                            </ul>
                            -->
						</nav>

				</header>
			</div>

		<!-- Banner -->
			<!-- <div id="banner-wrapper">
				<div id="banner" class="box container">
					<div class="row">
						<div class="7u">
							<h2>Hi. This is Verti.</h2>
							<p>It's a free responsive site template by HTML5 UP</p>
						</div>
						<div class="5u">
							<ul>
								<li><a href="#" class="button big icon fa-arrow-circle-right">Ok let's go</a></li>
								<li><a href="#" class="button alt big icon fa-question-circle">More info</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div> -->

		<!-- Features -->
		<!-- <div id="features-wrapper">
				<div class="container">
					<div class="row">
						<div class="4u">
					-->
							<!-- Box -->
					<!--			<section class="box feature">
									<a href="#" class="image featured"><img src="images/pic01.jpg" alt="" /></a>
									<div class="inner">
										<header>
											<h2>Put something here</h2>
											<p>Maybe here as well I think</p>
										</header>
										<p>Phasellus quam turpis, feugiat sit amet in, hendrerit in lectus. Praesent sed semper amet bibendum tristique fringilla.</p>
									</div>
								</section>

						</div>
						<div class="4u">
-->
							<!-- Box -->
<!--								<section class="box feature">
									<a href="#" class="image featured"><img src="images/pic02.jpg" alt="" /></a>
									<div class="inner">
										<header>
											<h2>An interesting title</h2>
											<p>This is also an interesting subtitle</p>
										</header>
										<p>Phasellus quam turpis, feugiat sit amet in, hendrerit in lectus. Praesent sed semper amet bibendum tristique fringilla.</p>
									</div>
								</section>

						</div>
						<div class="4u">

	-->						<!-- Box -->
	<!--							<section class="box feature last">
									<a href="#" class="image featured"><img src="images/pic03.jpg" alt="" /></a>
									<div class="inner">
										<header>
											<h2>Oh, and finally ...</h2>
											<p>Here's another intriguing subtitle</p>
										</header>
										<p>Phasellus quam turpis, feugiat sit amet in, hendrerit in lectus. Praesent sed semper amet bibendum tristique fringilla.</p>
									</div>
								</section>

						</div>
					</div>
				</div>
			</div>
-->
		<!-- Main -->
			<div id="main-wrapper">
				<div class="container">
					<div class="row">
						<div class="4u">

							<!-- Sidebar -->
								<!-- <div id="sidebar">
									<section class="widget thumbnails">
										<h3>Interesting stuff</h3>
										<div class="grid">
											<div class="row no-collapse 50%">
												<div class="6u"><a href="#" class="image fit"><img src="images/pic04.jpg" alt="" /></a></div>
												<div class="6u"><a href="#" class="image fit"><img src="images/pic05.jpg" alt="" /></a></div>
											</div>
											<div class="row no-collapse 50%">
												<div class="6u"><a href="#" class="image fit"><img src="images/pic06.jpg" alt="" /></a></div>
												<div class="6u"><a href="#" class="image fit"><img src="images/pic07.jpg" alt="" /></a></div>
											</div>
										</div>
										<a href="#" class="button icon fa-file-text-o">More</a>
									</section>
								</div>-->

						</div>
						<div class="8u important(collapse)">

							<!-- Content -->
								<div id="content">
                                    @section('content')
                                    @show
								</div>

						</div>
					</div>
				</div>
			</div>

		<!-- Footer -->
			<div id="footer-wrapper">
				<footer id="footer" class="container">
					<div class="row">
					<!--	<div class="3u">

								<section class="widget links">
									<h3>Random Stuff</h3>
									<ul class="style2">
										<li><a href="#">Etiam feugiat condimentum</a></li>
										<li><a href="#">Aliquam imperdiet suscipit odio</a></li>
										<li><a href="#">Sed porttitor cras in erat nec</a></li>
										<li><a href="#">Felis varius pellentesque potenti</a></li>
										<li><a href="#">Nullam scelerisque blandit leo</a></li>
									</ul>
								</section>

						</div>
						<div class="3u">

								<section class="widget links">
									<h3>Random Stuff</h3>
									<ul class="style2">
										<li><a href="#">Etiam feugiat condimentum</a></li>
										<li><a href="#">Aliquam imperdiet suscipit odio</a></li>
										<li><a href="#">Sed porttitor cras in erat nec</a></li>
										<li><a href="#">Felis varius pellentesque potenti</a></li>
										<li><a href="#">Nullam scelerisque blandit leo</a></li>
									</ul>
								</section>

						</div>
						<div class="3u">

								<section class="widget links">
									<h3>Random Stuff</h3>
									<ul class="style2">
										<li><a href="#">Etiam feugiat condimentum</a></li>
										<li><a href="#">Aliquam imperdiet suscipit odio</a></li>
										<li><a href="#">Sed porttitor cras in erat nec</a></li>
										<li><a href="#">Felis varius pellentesque potenti</a></li>
										<li><a href="#">Nullam scelerisque blandit leo</a></li>
									</ul>
								</section>

						</div> -->
						<div class="3u">

							<!-- Contact -->
								<!-- <section class="widget contact last">
									<h3>Contact Us</h3>
									<ul>
										<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
										<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
										<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
										<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
										<li><a href="#" class="icon fa-pinterest"><span class="label">Pinterest</span></a></li>
									</ul>
								</section>
                                -->

						</div>
					</div>
					<!-- <div class="row">
						<div class="12u">
							<div id="copyright">
								<ul class="menu">
									<li>&copy; Untitled. All rights reserved</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
								</ul>
							</div>
						</div>
                        -->
					</div>
				</footer>
			</div>

	</body>
</html>
