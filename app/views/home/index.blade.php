@extends('layouts.default')

@section('content')
<div class="home">
  <div class="main-banner">
      <div class="banner-wrap">
          <img src="/images/main-banner.gif" class="main-banner-image" alt="main-banner">
          <div class="banner-content">
              <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-6 mobile-center ">
                    </div>
                </div>
              </div>
          </div>
      </div>

      <div class="cases menu-divde">
      </div>

  </div>
</div>
@stop
