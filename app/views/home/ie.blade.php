@extends('layouts.ie')

@section('content')
<div class="browser">
	<div class="banner-wrap">
      <img src="/images/main-banner.jpg" class="main-banner-image" alt="main-banner">
      <div class="banner-content">
          <div class="container">
          	<img src="/images/arkade-logo-white.png" class="logo">
            <div class="row browser-wrap">
                <div class="col-sm-12">
                	<h1>Sorry, your browser is not supported.</h1>
										<h1>Please download the latest version of browser.</h1>
										<ul>
											<li>
												<a class="browser-icon" id="ie" href="http://windows.microsoft.com/en-au/internet-explorer/ie-10-worldwide-languages">
													<img src="/images/ie.png" />
												</a>
											</li>
											<li>
												<a class="browser-icon" id="firefox" href="http://getfirefox.com">
													<img src="/images/firefox.png" />
												</a>
											</li>
											<li>
												<a class="browser-icon" id="chrome" href="http://chrome.google.com">
													<img src="/images/chrome.png" />
												</a>
											</li>
											<li>
												<a class="browser-icon" id="safari" href="http://www.apple.com/safari/download">
													<img src="/images/safari.png" />
												</a>
											</li>
										</ul>
                </div>
            </div>
          </div>
      </div>
  </div>
</div>
@stop